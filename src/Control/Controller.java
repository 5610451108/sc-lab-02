package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Control.Controller;
import Control.Controller.AddInterestListener;
import Model.BankAccount;
import View.InvestmentFrame;

public class Controller 
	{
	private static final double INITIAL_BALANCE = 1000;
	public BankAccount bank_account = new BankAccount(INITIAL_BALANCE);
	public InvestmentFrame investmentframe = new InvestmentFrame();
	public ActionListener listener = new AddInterestListener();
	private double rate;
	private double bank_rate;
	
	public Controller() 
	{
		this.investmentframe.setListener(listener);
	}
	
	class AddInterestListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent event)
		{
			rate = investmentframe.getRate();
			bank_rate = bank_account.count(rate);
			bank_account.deposit(bank_rate);
			investmentframe.setOutput("" + bank_account.getBalance());
		}
	}
	
	public static void main(String[] args)
	{
		new Controller();
	}
}
