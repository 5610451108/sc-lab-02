package Model;

/**
   A bank account has a balance that can be changed by 
   deposits and withdrawals.
*/

public class BankAccount {
	
	 private double balance;
	 private double rate = balance;
	 /**
        Constructs a bank account with a zero balance.
     */
	 
	 public BankAccount()
	   {   
	      balance = 0;
	   }
	 
	 /**
        Constructs a bank account with a given balance.
        @param initialBalance the initial balance
     */

	 public BankAccount(double initialBalance)
	   {  
		   balance = initialBalance;
	   }
	 
	 public void deposit(double amount)
	   {  
	      double newBalance = balance + amount;
	      balance = newBalance;
	   }
	 /**
        Withdraws money from the bank account.
        @param amount the amount to withdraw
     */

	 public void withdraw(double amount)
	   {   
	      double newBalance = balance - amount;
	      balance = newBalance;
	   }
	 /**
        Gets the current balance of the bank account.
        @return the current balance
     */
	 
	 public double getBalance()
	   {   
	      return balance;
	   }
	  public double count(double rate)
	  {
		  this.rate = balance * rate / 100;
		  return this.rate;
	   }
}
